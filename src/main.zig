const std = @import("std");
const io = std.io;
const clap = @import("clap");

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const params = comptime clap.parseParamsComptime(
        \\-h, --help             Display this help and exit.
        \\<string>...
        \\
    );
    var diagnostic = clap.Diagnostic{};
    const res = clap.parse(clap.Help, &params, clap.parsers.default, .{
        .diagnostic = &diagnostic,
        .allocator = allocator,
    }) catch |err| {
        diagnostic.report(io.getStdErr().writer(), err) catch {};
        return;
    };

    // open and read files
    var contents = std.ArrayList([]const u8).init(allocator);
    var content_total: usize = 0;
    for (res.positionals) |pos| {
        const content = try file_open(pos, allocator);
        try contents.append(content);
        content_total += content.len;
    }

    // extract host names (using fixed buffer allocator)
    const buf_fixed = try allocator.alloc(u8, content_total); // TODO how much?
    var fba = std.heap.FixedBufferAllocator.init(buf_fixed);
    var hosts = std.ArrayList([]const u8).init(fba.allocator());
    for (contents.items) |content| {
        try file_extract_names(content, &hosts);
    }

    // write to stdout
    const stdout = std.io.getStdOut().writer();
    var buf = std.io.bufferedWriter(stdout);
    var bw = buf.writer();
    for (hosts.items) |host| {
        try bw.writeAll(host);
        try bw.writeAll(" ");
    }
    try bw.writeAll("\n");
    try buf.flush();
}

const MAXSIZE = 10 * 1024 * 1024 * 1024;

fn file_open(filename: []const u8, allocator: std.mem.Allocator) ![]const u8 {
    const abspath = std.fs.realpathAlloc(allocator, filename) catch |err| switch (err) {
        error.FileNotFound => {
            std.log.err("File not found: {s}", .{filename});
            return err;
        },
        else => {
            std.log.err("other error", .{});
            return err;
        },
    };
    defer allocator.free(abspath);

    const f = try std.fs.openFileAbsolute(abspath, .{ .mode = .read_only });
    defer f.close();

    return try f.readToEndAlloc(allocator, MAXSIZE);
}

fn file_extract_names(content: []const u8, host_list: *std.ArrayList([]const u8)) !void {
    var iter = std.mem.splitScalar(u8, content, '\n');
    while (iter.next()) |line| {
        if (line_extract_hostname(line)) |hostname| {
            try host_list.append(hostname);
        }
    }
}

fn line_extract_hostname(line: []const u8) ?[]const u8 {
    var iter = std.mem.tokenizeAny(u8, line, " \t");
    const host = iter.next() orelse return null;
    // skip 2 fields
    _ = iter.next() orelse return null;
    _ = iter.next() orelse return null;
    const record_type = iter.next() orelse return null;
    if (!std.mem.eql(u8, record_type, "CNAME") and !std.mem.eql(u8, record_type, "A") and !std.mem.eql(u8, record_type, "AAAA")) {
        return null;
    }
    if (std.mem.startsWith(u8, host, "*")) {
        return null;
    }
    return std.mem.trimRight(u8, host, ".");
}

test "nonmatching line" {
    try std.testing.expectEqual(null, line_extract_hostname("foo"));
}

test "matching line" {
    const line = "abel.agdsn.network.\t3600\tIN\tA\t10.144.0.11";
    const expected = "abel.agdsn.network";
    const extracted = line_extract_hostname(line);
    try std.testing.expect(extracted != null);
    try std.testing.expectEqualStrings(expected, extracted.?);
}

test "test wildcard CNAMEs are ignored" {
    const line = "*.k8s.agdsn.de.\t3600\tIN\tCNAME\tk8s-ingress.agdsn.de.";
    try std.testing.expectEqual(null, line_extract_hostname(line));
}

test "test zonefile" {
    const content = @embedFile("test.zone");
    var hosts = std.ArrayList([]const u8).init(std.testing.allocator);
    defer hosts.deinit();
    var hosts_expected = std.ArrayList([]const u8).init(std.testing.allocator);
    defer hosts_expected.deinit();
    try hosts_expected.appendSlice(&[_][]const u8{
        "_dmarc.agdsn.network",
        "abel.agdsn.network",
        "aruba-mobility-master-1.agdsn.network",
        "aruba-mobility-master-2.agdsn.network",
        "aruba-mobility-master-mgmt.agdsn.network",
        "aruba-mobility-master-primary-mgmt.agdsn.network",
        "aruba-mobility-master-secondary-mgmt.agdsn.network",
    });
    try file_extract_names(content, &hosts);
    try std.testing.expectEqual(hosts_expected.items.len, hosts.items.len);
    for (hosts_expected.items, hosts.items) |e, a| {
        try std.testing.expectEqualStrings(e, a);
    }
}
